import axios from 'axios'
import api from './api'
import store from '../store'

export function request(action, params) {
  return new Promise((resolve, reject) => {
    if (params != null) { params.auth = store.state.hulaShopAuth }
    axios({
      method: 'post',
      url: api.ApiRootUrl + action,
      data: params
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

export default {
  postData(action, params) {
    return request(action, params)
  }
}
