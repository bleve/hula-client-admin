import api from './api'
import utils from './utils'

const fetchCategoriesList = (page, name, status) => {
  return new Promise((resolve, reject) => {
    if (status.length == 0) { status = null }
    const request = utils.postData(api.adminFetchCategoriesByPage, {
      page,
      name,
      status
    })
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

const insertCategories = (name, imgUrl) => {
  return new Promise((resolve, reject) => {
    const request = utils.postData(api.adminInsertCategories, {
      name,
      imgUrl
    })
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

const updateCategories = (categoriesId, name, imgUrl) => {
  return new Promise((resolve, reject) => {
    const request = utils.postData(api.adminUpdateCategories, {
      categoriesId,
      name,
      imgUrl
    })
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

const updateStatus = (categoriesId, status) => {
  return new Promise((resolve, reject) => {
    const request = utils.postData(api.adminUpdateCategories, {
      categoriesId,
      status
    })
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

export default {
  fetchCategoriesList,
  insertCategories,
  updateCategories,
  updateStatus
}
