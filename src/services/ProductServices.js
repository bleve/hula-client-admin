import api from './api'
import utils from './utils'

const fetchProductList = (page, title, status, orderBy) => {
  return new Promise((resolve, reject) => {
    if (status.length == 0) { status = null }
    const request = utils.postData(api.adminFetchProductByPage, {
      page,
      title,
      status,
      orderBy
    })
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

const updateStatus = (productId, name, imgUrl) => {
  return new Promise((resolve, reject) => {
    const request = utils.postData(api.adminUpdateProduct, {
      productId,
      name,
      imgUrl
    })
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

export default {
  fetchProductList,
  updateStatus
}
