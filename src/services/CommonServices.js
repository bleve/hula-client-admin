
import api from './api'
import utils from './utils'

const uploadFile = (formData) => {
  return new Promise((resolve, reject) => {
    const request = utils.postData(api.adminUpload, formData)
    request.then(res => {
      resolve(res.data.data)
    }).catch(err => {
      reject(err)
    })
  })
}

export default {
  uploadFile
}
