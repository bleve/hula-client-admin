const ApiRootUrl = 'https://kaidian.weboostapp.com/'

export default {
  ApiRootUrl,

  // basic operation
  adminLogin: 'api/public/api/v1/adminLogin',
  adminUpload: 'api/public/api/v1/adminUpload',

  // product
  adminFetchProductByPage: 'api/public/api/v1/adminFetchProductByPage',
  adminUpdateProduct: 'api/public/api/v1/adminUpdateProduct',

  // categories
  adminFetchCategoriesByPage: 'api/public/api/v1/adminFetchCategoriesByPage',
  adminInsertCategories: 'api/public/api/v1/adminInsertCategories',
  adminUpdateCategories: 'api/public/api/v1/adminUpdateCategories'

}
